const USERS_URL = "https://ajax.test-danit.com/api/json/users";
const POSTS_URL = "https://ajax.test-danit.com/api/json/posts";
const IMGS_URL = "https://ajax.test-danit.com/api/json/photos";

const container = document.querySelector(".container");

class UserCard {

  async sendRequest(url, method = "GET", config = {}) {
    try {
      const response = await fetch(url, { method, ...config });
      if (!response.ok) throw new Error(`Failed to fetch from ${url}`);
      return response.json();
    } catch (error) {
      console.error("Error fetching data:", error);
      throw error;
    }
  }

  async createCards() {
    try {
      const [users, posts, imgs] = await Promise.all([
        this.sendRequest(USERS_URL),
        this.sendRequest(POSTS_URL),
        this.sendRequest(IMGS_URL),
      ]);

      posts.forEach(({ userId, title, body, id }) => {
        const user = users.find((user) => user.id === userId);
        const img = imgs.find((img) => img.id === userId);
 
        const postElement = document.createElement("div");
        postElement.className = "card";
        postElement.dataset.userId = id;
        postElement.innerHTML = `
        <div class="card__ava">
          <div class="ava__box">
            <img class="ava__img" src="${img.thumbnailUrl}" alt="" />
          </div>
        </div>
        <div class="content">
          <p class="contant__name">${user.name}
            <span> • </span>
            <span class="contant__username">@${user.username}</span></p>
          <p class="contant__company">${user.company.name}</p>
          <p class="contant__title">Post Title: <span class="contant__title-text">${title}.</span></p>
          <p class="contant__publication">${body}.</p>
        </div>
        <button class="btn btn_close">X</button>
        `;
        container.append(postElement);
        
        postElement.addEventListener("click", (event)=>{
          const del = event.target.closest(".btn_close")
          // console.log(del);
          if(!del) {
            return
          }
          del.closest(".card").remove();
          // if(del) {
          //   const cardId = del.closest(".card").dataset.userId;
          // }
        })
      });
    } catch (error) {
      console.error("Some mistake:", error);
    }
  }
}

const userCard = new UserCard();
userCard.createCards();
